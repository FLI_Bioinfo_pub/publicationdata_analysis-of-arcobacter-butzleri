**Resistance profiling of *Aliarcobacter butzleri* using two databases: ARCO_IBIZ_AMR and ARCO_IBIZ_VIRULENCE**

These two databases were constructed using previously described antimicrobial/heavy metal resistance and virulence genes as well as resistance genes found in this study. ABRicate (https://github.com/tseemann/abricate) was used for creating these databases. 
The nucleotide sequences of all the genes are provided in "AMR_heavy_metal_database.fasta" and "Virulence_database.fasta".

**AMR and heavy metal resistance database for *Aliarcobacter butzleri* (ARCO_IBIZ_AMR)**

This database contains 92 AMR genes and 27 heavy metal resistance genes, including genes for efflux pump systems, putative copper cluster and other antibiotic determinants.
A list of these genes is provided here:

|gene	|alias	|Prokka locus	|Prokka annotation	|RefSeq locus	|RefSeq annotation	|Gene length (bp)|
|---	|---	|--- 	|--- 	|--- 	|--- 	|--- |
|mdtB1	|EP1	|RM4018p_04610	|Multidrug resistance protein MdtB	|ABU_RS02300	|efflux RND transporter permease subunit	|3,027 |
|czcB	|EP1 |RM4018p_04620	|Cobalt-zinc-cadmium resistance protein CzcB	|ABU_RS02305	|Transporter	|693 |
|- |EP1 |RM4018p_04630	|Hypothetical protein	|ABU_RS02310	|TolC family protein	|1,221 |
|fadR	|EP1 |RM4018p_04640	|Fatty acid metabolism regulator protein	|ABU_RS02315	|TetR/AcrR family transcriptional regulator	|597 |
|yajR	|EP2	|RM4018p_05880	|Inner membrane transport protein YajR	|ABU_RS02930	|MFS transporter	|1,314 |
|ohrR	|-	|RM4018p_06000	|Organic hydroperoxide resistance transcriptional regulator ohrR	|ABU_RS02990	|MarR family transcriptional regulator	|432 |
|-	|EP3	|RM4018p_07090	|Hypothetical protein	|ABU_RS03530	|TolC family protein	|1,215 |
|macA1	|EP3 |RM4018p_07100	|Macrolide export protein MacA	|ABU_RS03535	|Efflux RND transporter periplasmic adaptor subunit	|1,263 |
|yknY	|EP3 |RM4018p_07110	|putative ABC transporter ATP-binding protein YknY	|ABU_RS03540	|ABC transporter ATP-binding protein	|705 |
|macB2	|EP3 |RM4018p_07120	|Macrolide export ATP-binding/permease protein MacB	|ABU_RS03545	|FtsX-like permease family protein	|1,212 |
|kstR2	|-	|RM4018p_08180	|HTH-type transcriptional repressor KstR2	|ABU_RS04070	|TetR/AcrR family transcriptional regulator	|594 |
|bepE	|EP4	|RM4018p_08190	|Efflux pump membrane transporter BepE	|ABU_RS04075	|Multidrug efflux RND transporter permease subunit	|3,129 |
|bepD	|EP4	|RM4018p_08200	|Efflux pump periplasmic linker BepD	|ABU_RS04080	|Efflux RND transporter periplasmic adaptor subunit	|1,059 |
|-	|EP4	|RM4018p_08210	|Hypothetical protein	|ABU_RS04085	|TolC family protein	|1,236 |
|mprA3	|EP4	|RM4018p_08220	|Transcriptional repressor MprA	|ABU_RS04090	|MarR family transcriptional regulator	|483 |
|msbA	|EP5	|RM4018p_08810	|Lipid A export ATP-binding/permease protein MsbA	|ABU_RS04390	|ABC transporter ATP-binding protein	|1,710 |
|uup	|EP6	|RM4018p_09130	|ABC transporter ATP-binding protein uup	|ABU_RS04550	|ABC-F family ATP-binding cassette domain-containing protein	|1,950 |
|ttgB	|EP7	|RM4018p_09960	|Toluene efflux pump membrane transporter TtgB	|ABU_RS04960	|efflux RND transporter permease subunit	|1,656 |
|mdtB2	|EP7	|RM4018p_09970	|Multidrug resistance protein MdtB	|ABU_RS04965	|efflux RND transporter permease subunit	|1,563 |
|mdtE	|EP7	|RM4018p_09980	|Multidrug resistance protein MdtE	|ABU_RS04970	|efflux RND transporter periplasmic adaptor subunit	|726 |
|-	|EP7	|RM4018p_09990	|Hypothetical protein	|ABU_RS04975	|TolC family protein	|1,353 |
|bvgS	|T1SS	|RM4018p_10320	|Virulence sensor protein BvgS	|ABU_RS05140	|transporter substrate-binding domain-containing protein	|1,575 |
|sphR	|T1SS	|RM4018p_10330	|Alkaline phosphatase synthesis transcriptional regulatory protein SphR	|ABU_RS05145	|response regulator transcription factor	|675 |
|-	|T1SS	|RM4018p_10340	|Hypothetical protein	|ABU_RS05150	|TolC family protein	|1,800 |
|ttgD	|T1SS	|RM4018p_10350	|Toluene efflux pump periplasmic linker protein TtgD	|ABU_RS05155	|Efflux RND transporter periplasmic adaptor subunit	|714 |
|-	|T1SS	|RM4018p_10360	|Hypothetical protein	|ABU_RS05160	|DUF4347 domain-containing protein	|7,554 |
|macA2	|T1SS	|RM4018p_10370	|Macrolide export protein MacA	|ABU_RS05165	|Efflux RND transporter periplasmic adaptor subunit	|1,347 |
|-	|T1SS	|RM4018p_10380	|Hypothetical protein	|ABU_RS05170	|HlyD family efflux transporter periplasmic adaptor subunit	|2,121 |
|sugE	|EP8	|RM4018p_10550	|Quaternary ammonium compound-resistance protein SugE	|ABU_RS05255	|Quaternary ammonium compound efflux SMR transporter SugE	|318 |
|fsr	|EP9	|RM4018p_11960	|Fosmidomycin resistance protein	|ABU_RS05950	|MFS transporter	|1,155 |
|hspR	|EP10	|RM4018p_13670	|Putative heat shock protein HspR	|ABU_RS06795	|helix-turn-helix transcriptional regulator	|381 |
|macA3	|EP10	|RM4018p_13680	|Macrolide export protein MacA	|ABU_RS06800	|HlyD family efflux transporter periplasmic adaptor subunit	|993 |
|ybhF	|EP10	|RM4018p_13690	|putative ABC transporter ATP-binding protein YbhF	|ABU_RS06805	|ABC transporter ATP-binding protein	|1,701 |
|ybhS	|EP10	|RM4018p_13700	|Inner membrane transport permease YbhS	|ABU_RS06810	|ABC transporter permease	|1,089 |
|ybhR	|EP10	|RM4018p_13710	|Inner membrane transport permease YbhR	|ABU_RS06815	|ABC transporter permease	|1,092 |
|mexB1	|EP11	|RM4018p_18760	|Multidrug resistance protein MexB	|ABU_RS09310	|efflux RND transporter permease subunit	|3,081 |
|-	|EP11	|RM4018p_18770	|Hypothetical protein	|ABU_RS09315	|Hypothetical protein	|699 |
|-	|EP11	|RM4018p_18780	|Hypothetical protein	|ABU_RS09320	|TolC family protein	|1,158 |
|-	|EP12	|RM4018p_19480	|Hypothetical protein	|ABU_RS09670	|MFS transporter	|1,185 |
|ydhP	|EP13	|RM4018p_20020	|Inner membrane transport protein YdhP	|ABU_RS09940	|MFS transporter	|1,146|
|bcr1	|EP14	|RM4018p_21670	|Bicyclomycin resistance protein	|ABU_RS10755	|multidrug efflux MFS transporter	|1,092 |
|pleD5	|EP15	|RM4018p_21690 	|Response regulator PleD	|ABU_RS10765	|Diguanylate cyclase	|1,746 |
|mexA1	|EP15	|RM4018p_21700	|Multidrug resistance protein MexA	|ABU_RS10770	|Efflux RND transporter periplasmic adaptor subunit	|1,137 |
|mexB2	|EP15	|RM4018p_21710	|Multidrug resistance protein MexB	|ABU_RS10775	|Multidrug efflux RND transporter permease subunit	|3,126 |
|ttgC	|EP15	|RM4018p_21720	|putative efflux pump outer membrane protein TtgC / OprM	|ABU_RS10780	|efflux transporter outer membrane subunit	|1,401 |
|oprM	|EP16	|RM4018p_22330	|Outer membrane protein OprM	|ABU_RS11085	|efflux transporter outer membrane subunit	|1,389 |
|mexB3	|EP16	|RM4018p_22340	|Multidrug resistance protein MexB	|ABU_RS11090	|Multidrug efflux RND transporter permease subunit	|3,168 |
|mexA2	|EP16	|RM4018p_22350	|Multidrug resistance protein MexA	|ABU_RS11095	|Efflux RND transporter periplasmic adaptor subunit	|1,155 |
|-	|EP16	|RM4018p_22360	|Hypothetical protein	|ABU_RS11100	|TetR/AcrR family transcriptional regulator	|540 |
|bcr2	|EP17	|RM4018p_22370	|Bicyclomycin resistance protein	|ABU_RS11105	|multidrug efflux MFS transporter	|1,191 |
|cdhR	|EP18	|BMH_AB_233Bp_03110	|HTH-type transcriptional regulator CdhR	|-	|-	|918 |
|bepF	|EP18	|BMH_AB_233Bp_03120	|Efflux pump periplasmic linker BepF	|-	|-	|1,155 |
|bepE1	|EP18	|BMH_AB_233Bp_03130	|Efflux pump membrane transporter BepE	|-	|-	|3,132 |
|oprM1	|EP18	|BMH_AB_233Bp_03140	|Outer membrane protein OprM	|-	|-	|1,422 |
|cnrA	|EP19	|BMH_AB_246Bp_07320	|Nickel and cobalt resistance protein CnrA	|-	|-	|3,057 |
|mdtA	|EP19	|BMH_AB_246Bp_07330	|Multidrug resistance protein MdtA	|-	|-	|1,077 |
|-	|EP19	|BMH_AB_246Bp_07340	|Putative HTH-type transcriptional regulator	|-	|-	|531 |
|oprM1	|EP19	|BMH_AB_246Bp_07350	|Outer membrane protein OprM	|-	|-	|1,455 |
|tolC	|-	|Ab_2211p_17880	|Outer membrane protein TolC	|-	|-	|1,278 |
|-	|-	|RM4018p_09950	|Hypothetical protein	|ABU_RS04955	|Antibiotic resistance protein	|600 |
|-	|-	|RM4018p_03390	|putative multidrug export ATP-binding/permease protein	|ABU_RS01690	|ABC transporter ATP-binding protein	|1,782 |
|-	|-	|RM4018p_11130	|putative multidrug export ATP-binding/permease protein	|ABU_RS05540	|ABC transporter ATP-binding protein	|1,770 |
|-	|-	|RM4018p_04700	|putative multidrug export ATP-binding/permease protein	|ABU_RS02345	|ABC transporter ATP-binding protein	|1,638 |
|acrB	|acrB	|RM4018p_03960	|multidrug efflux pump subunit AcrB	|ABU_RS01975	|protein translocase subunit SecD	|1,566 |
|arnA	|arnA	|BMH_AB_233Bp_21330	|Bifunctional polymyxin resistance protein	|-	|-	|789 |
|arnB	|arnB	|RM4018p_12550	|UDP-4-amino-4-deoxy-L-arabinose--oxoglutarate aminotransferase	|ABU_RS06230	|DegT/DnrJ/EryC1/StrS family aminotransferase	|1,137 |
|bla1	|bla1	|RM4018p_05810	|Metallo-beta-lactamase type 2	|ABU_RS02895	|MBL fold metallo-hydrolase	|918 |
|bla2	|bla2	|RM4018p_13040	|Putative metallo-hydrolase	|ABU_RS06485	|MBL fold metallo-hydrolase	|594 |
|bla3	|bla3	|RM4018p_14830	|Beta-lactamase OXA-15	|ABU_RS07375	|Class D beta-lactamase	|762 |
|cat3	|cat	|RM4018p_07870	|Chloramphenicol acetyltransferase 3	|ABU_RS03915	|Type A chloramphenicol O-acetyltransferase	|645 
|eptA	|eptA	|RM4018p_23280	|phosphoethanolamine transferase	|ABU_RS11565	|phosphoethanolamine--lipid A transferase	|1,611 |
|gyrA	|-	|RM4018p_18010	|DNA gyrase subunit A	|ABU_RS08945	|DNA gyrase subunit A	|2,583 |
|hcpC	|hcpC	|RM4018p_05610	|putative beta-lactamase	|ABU_RS02795	|sel 1 repeat family protein	|729 |
|hipA1	|hlpA	|D4963p_05780	|Serine/threonine-protein kinase 	|-	|-	|1,371 |
|hipA2	|hlpA	|D4963p_10590	|Serine/threonine-protein kinase 	|-	|-	|1,2778 |
|hipA3	|hlpA	|D4963p_10610	|Serine/threonine-protein kinase 	|-	|-	|327 |
|hipA4	|hlpA	|D4963p_10620	|Serine/threonine-protein kinase 	|-	|-	|1,014 |
|macB1	|macB	|RM4018p_04420	|Macrolide export ATP-binding/permease protein MacB	|ABU_RS02205	|FtsX-like permease family protein	|1,083 |
|mrdA	|-	|RM4018p_10020	|Penicillin-binding protein 2	|ABU_RS04990	|Penicillin-binding protein 2	|1,782 |
|pbpB	|-	|RM4018p_19240	|Penicillin-binding protein 	|ABU_RS09545	|Penicillin-binding protein 2	|1,863 |
|pbpF	|-	|RM4018p_19150	|Penicillin-binding protein 1F	|ABU_RS09500	|PBP 1A family penicillin-binding protein	|1,980 |
|relE	|relE	|RM4018p_13910	|Hypothetical protein	|ABU_RS06915	 |type II toxin-antitoxin system RelE/ParE family toxin	|291 |
|rlmN	|rlmN	|RM4018p_00580	|putative dual specificity RNA methyltransferase	|ABU_RS00285	|23S rRNA (adenine (2503)-C(2))-methyltransferase RlmN	|1,083 |
|rplD	|-	|RM4018p_07580	|50S ribosomal protein L4	|ABU_RS03770	|50S ribosomal protein L4	|612 |
|rplV	|-	|RM4018p_07620	|50S ribosomal protein L22	|ABU_RS03790	|50S ribosomal protein L22	|333 |
|sttH	|sttH	|RM4018p_02650	|Streptothricin hydrolase	|ABU_RS01320	|cysteine hydrolase	|558 ||
|tetA	|tetA	|RM4018p_03370	|Tetracycline resistance protein, Class C	|ABU_RS01680	|MFS transporter	|1,176 |
|tolC	|tolC	|RM4018p_00650	|Hypothetical protein	|ABU_RS00315	|TolC family protein	|1,188 |
|oprF3	|-	|RM4018p_11260	|Outer membrane porin F 	|ABU_RS05605	|TolC family outer membrane protein	|2,565 |
|vatD	|-	|RM4018p_06870	|Streptogramin A acetyltransferase	|ABU_RS03420	|acyltransferase	|534 |
|wbpD	|wbpD	|RM4018p_06670	|UDP-2-acetamido-3-amino-2,3-dideoxy-D-glucuronate N-acetyltransferase	|ABU_RS03320	|N-acetyltransferase	|579 |
|ybiT1	|-	|RM4018p_10700	|putative ABC transporter ATP-binding protein	|ABU_RS05330	|ABC-F family ATP-binding cassette domain-containing protein	|1,599 |
|ybiT2	|-	|RM4018p_11470	|putative ABC transporter ATP-binding protein	|ABU_RS05710	|ABC-F family ATP-binding cassette domain-containing protein	|1,488 |
|ylmA	|-	|RM4018p_10450	|putative ABC transporter ATP-binding protein YlmA	|ABU_RS05205	|ATP-binding cassette domain-containing protein	|789 |
|aseR	|-	|RM4018p_05620	|Hypothetical protein	|ABU_RS02800	|arcsenic resistance protein	|312 |
|arsB	|arsB	|RM4018p_19420	|Arsenical pump membrane protein	|ABU_RS09640	|arsenic transporter	|1,251 |
|arsC1	|arsC	|RM4018p_13890	|Arsenate reductase	|ABU_RS06905	|arsenate reductase (glutaredoxin)	|351 |
|arsC2	|arsC	|RM4018p_19460	|Glutaredoxin arsenate reductase	|ABU_RS09660	|arsenate reductase ArsC	|399 |
|cadA	|cadA	|RM4018p_18720	|Cadmium, zinc and cobalt-transporting ATPase	|ABU_RS09290	|cadmium-translocating P-type ATPase	|2,124 |
|cnrA	|-	|BMH_AB_246Bp_07320	|Nickel and cobalt resistance protein CnrA	|-	|-	|3,057 |
|copA1	|copA_1	|RM4018p_04810	|Copper-exporting P-type ATPase A	|ABU_RS02400	|heavy metal translocating P-type ATPase	|2,520 |
|copA2	|copA_2	|RM4018p_05490	|putative copper-importing P-type ATPase A	|ABU_RS02735	|heavy metal translocating P-type ATPase	|2,439 |
|copR	|-	|RM4018p_07970	|transcriptional activator protein CopR	|ABU_RS03965	|response regulator transcription factor	|663 |
|copZ	|copZ	|RM4018p_15130	|copper chaperone CopZ	|ABU_RS07520	|heavy-metal-associated domain-containing protein	|282 |
|corC	|corC	|RM4018p_11540	|magnesium and cobalt efflux protein CorC	|ABU_RS05745	|HlyC/CorC family transporter	|1,338 |
|csoR	|csoR	|RM4018p_04800	|copper sensing transcriptional repressor	|ABU_RS02395	|metal-sensing transcriptional repressor	|267 |
|ctpC	|-	|RM4018p_07160	|putative manganese/ zinc-exporting P-type ATPase	|ABU_RS03560	|heavy metal translocating P-type ATPase	|2,112 |
|cusS	|cusS	|Ab_4511p_10200	|Sensor kinase CusS	|-	|-	|1,377 |
|czcA	|-	|Ab_2211p_12350	|Cobalt-zinc-cadmium resistance protein CzcA	|-	|-	|3,078 |
|czcB	|czcB	|RM4018p_04620	|Cobalt-zinc-cadmium resistance protein CzcB	|ABU_RS02305	|Transporter	|693 |
|czcD	|czcD	|RM4018p_03450	|cadmium, cobalt and zinc/ H+-K+ antiporter	|ABU_RS01720	|cation diffusion facilitator family transporter	|1,056 |
|czcR1	|-	|RM4018p_04350	|Transcriptional activator protein CzcR	|ABU_RS02170	|response regulator transcription factor	|678 |
|czcR2	|-	|RM4018p_09080	|Transcriptional activator protein CzcR	|ABU_RS04525	|response regulator transcription factor	|663 |
|czcR3	|-	|RM4018p_10900	|Transcriptional activator protein CzcR	|ABU_RS05425	|response regulator transcription factor	|663 |
|czcR4	|-	|RM4018p_11730	|Transcriptional activator protein CzcR	|ABU_RS05840	|response regulator transcription factor	|666 |
|czcR5	|-	|Ab_2211p_22630	|Transcriptional activator protein CzcR	|-	|-	|666 |
|merT	|merT	|RM4018p_15140	|Hypothetical protein	|ABU_RS07525	|Transporter	|327 |
|modA	|modA	|RM4018p_00120	|Molybdate-binding periplasmatic protein	|ABU_RS00060	|molybdate ABC transporter substrate-binding protein	|747 |
|modB	|modB	|RM4018p_00100	|Molybdenum transport system permease protein	|ABU_RS00050	|molybdate ABC transporter permease subunit	|708 |
|mopA	|mopA	|RM4018p_00130	|Molybdenum-pterin-binding protein	|ABU_RS00065	|LysR family transcriptional regulator	|783 |
|zntB	|zntB	|RM4018p_22910	|Zinc transport protein	|ABU_RS11380	|zinc transporter ZntB	|966 |

**Virulence database for *Aliarcobacter butzleri* (ARCO_IBIZ_VIRULENCE)**

This database contains 148 virulence-associated genes, including flagellar genes, chemotaxis system genes, urease cluster genes, putative capsule cluster genes, type IV secretion system genes, lipid A cluster genes, and other virulence genes relevant for adherence, invasion, and iron absorption. A list of these genes is provided here:

|gene	|alias	|Prokka locus	|Prokka annotation	|RefSeq locus	|RefSeq annotation	|Gene length (bp)|
|---	|---	|--- 	|--- 	|--- 	|--- 	|--- |
|flaA	|flaA	|RM4018p_22650	|Flagellar filament 33 kDa core protein	|ABU_RS11245	|Flagellin	|924 |
|flaB	|flaB	|RM4018p_22660	|Flagellar filament 33 kDa core protein	|ABU_RS11250	|Flagellin	|924 |
|flgB	|flgB	|RM4018p_19750	|Flagellar basal body rod protein FlgB	|ABU_RS09805	|Flagellar basal body rod protein FlgB	|399 |
|flgC	|flgC	|RM4018p_19560	|Flagellar basal-body rod protein FlgC	|ABU_RS09710	|Flagellar basal body rod protein FlgC	|465 |
|flgD	|flgD	|RM4018p_19700	|Hypothetical protein	|ABU_RS09780	|Flagellar basal body rod modification protein FlgD	|681 |
|flgE1	|flgE1	|RM4018p_19680	|Flagellar hook protein FlgE	|ABU_RS09770	|Flagellar hook basal body complex protein 	|2,172 |
|flgE2	|flgE2	|RM4018p_19690	|Flagellar hook protein FlgE	|ABU_RS09775	|Flagellar hook protein FlgE	|1,752 |
|flgG1	|flgG1	|RM4018p_19760	|Flagellar basal-body rod protein FlgG	|ABU_RS09810	|Flagellar hook-basal body protein	|720 |
|flgG2	|flgG2	|RM4018p_19790	|Flagellar basal-body rod protein FlgG	|ABU_RS09825	|Flagellar basal-body rod protein FlgG	|789 |
|flgH	|flgH	|RM4018p_02100	|Flagellar L-ring protein	|ABU_RS01045	|Flagellar basal body L-ring protein FlgH	|705 |
|flgI	|flgl	|RM4018p_02030	|Flagellar P-ring protein	|ABU_RS01010	|Flagellar basal body P-ring protein FlgI	|1,050 |
|flgK	|flgK	|RM4018p_02110	|Flagellar hook-associated protein 1	|ABU_RS01050	|Flagellar hook-associated protein FlgK	|1,911 |
|flgL	|flgL	|RM4018p_19500	|Hypothetical protein	|ABU_RS09680	|Flagellar hook-associated protein 3	|1,131 |
|flhA	|flhA	|RM4018p_19490	|Flagellar biosynthesis protein FlhA	|ABU_RS09675	|Flagellar biosynthesis protein FlhA	|2,112 |
|flhB1	|-	|RM4018p_13940	|Flagellar biosynthetic protein FlhB	|ABU_RS06930	|Hypothetical protein	|285 |
|flhB2	|flhB	|RM4018p_19530	|Flagellar biosynthetic protein FlhB	|ABU_RS09695	|Flagellar biosynthesis protein FlhB	|1,050 |
|flhF	|flhF	|RM4018p_19600	|Flagellar biosynthesis protein FlhF	|ABU_RS09730	|Flagellar biosynthesis protein FlhF	|1,152 |
|fliD	|fliD	|RM4018p_02120	|Flagellar hook-associated protein 2	|ABU_RS01055	|Flagellar hook protein FliD	|1,338 |
|fliE	|fliE	|RM4018p_19570	|Flagellar hook-basal body complex protein FliE	|ABU_RS09715	|Flagellar hook-basal body complex protein FliE	|321 |
|fliF	|fliF	|RM4018p_19740	|Flagellar M-ring protein	|ABU_RS09800	|Flagellar M-ring protein FliF	|1,725 |
|fliG	|fliG	|RM4018p_19730	|Flagellar motor switch protein FliG	|ABU_RS09795	|Flagellar motor switch protein FliG	|1,011 |
|fliH	|fliH	|RM4018p_19720	|Hypothetical protein	|ABU_RS09790	|Flagellar assembly protein FliH	|726 |
|fliI	|fliI	|RM4018p_19510	|Flagellum-specific ATP synthase	|ABU_RS09685	|Flagellar protein export ATPase FliI	|1,308 |
|fliK	|fliK	|RM4018p_19550	|Hypothetical protein	|ABU_RS09705	|Flagellar hook-length control protein FliK	|1,938 |
|fliL	|fliL	|RM4018p_02080	|Hypothetical protein	|ABU_RS01035	|Flagellar basal body-associated FliL family protein	|528 |
|fliM	|fliM	|RM4018p_02020	|Flagellar motor switch protein FliM	|ABU_RS01005	|Flagellar motor switch protein FliM	|1,107 |
|fliN1	|fliY	|RM4018p_19640	|Flagellar motor switch protein FliN	|ABU_RS09750	|Flagellar motor switch protein FliY	|870 |
|fliN2	|fliN	|RM4018p_19710	|Flagellar motor switch protein FliN	|ABU_RS09785	|Flagellar motor switch protein FliN	|288 |
|fliP	|fliP	|RM4018p_10010	|Flagellar biosynthetic protein FliP	|ABU_RS04985	|Flagellar type III secretion system pore protein FliP	|726 |
|fliQ	|fliQ	|RM4018p_02050	|Flagellar biosynthetic protein FliQ	|ABU_RS01020	|Flagellar biosynthesis protein FliQ	|261 |
|fliR	|fliR	|RM4018p_19540	|Hypothetical protein	|ABU_RS09700	|Flagellar biosynthetic protein FliR	|750 |
|fliS	|fliS	|RM4018p_02130	|Flagellar protein FliS	|ABU_RS01060	|Flagellar export chaperone FliS	|357 |
|fliW2	|-	|RM4018p_02410	|Flagellar assembly factor FliW2	|ABU_RS01200	|FliW protein	|378 |
|hag	|hag	|RM4018p_11190	|Flagellin	|ABU_RS05570	|Flagellin	|816 |
|pomA	|motA	|RM4018p_04010	|Chemotaxis protein PomA	|ABU_RS02000	|Flagellar motor stator protein MotA	|759 |
|motB	|motB	|RM4018p_04000	|Motility protein B	|ABU_RS01995	|Flagellar motor protein MotB	|756 |
|cheW	|cheW	|RM4018p_04270	|Chemotaxis protein CheW	|ABU_RS02130	|chemotaxis protein CheW	|480 |
|cheV	|cheV	|RM4018p_06200	|Chemotaxis protein CheV	|ABU_RS03090	|response regulator	|909 |
|cheY1	|cheY1	|RM4018p_11900	|Chemotaxis protein CheY	|ABU_RS05920	|response regulator	|369 |
|cheA	|cheA	|RM4018p_11910	|Chemotaxis protein CheA	|ABU_RS05925	|chemotaxis protein CheA	|2,058 |
|cheR	|cheR	|RM4018p_11920	|Chemotaxis protein methyltransferase	|ABU_RS05930	|protein-glutamate O-methyltransferase CheR	|843 |
|cheB	|cheB	|RM4018p_11940	|Chemotaxis response regulator protein-glutamate methylesterase	|ABU_RS05940	|chemotaxis response regulator protein-glutamate methylesterase	|1,080 |
|cheY2	|cheY2	|RM4018p_13630	|Chemotaxis protein CheY	|ABU_RS06775	|response regulator	|369 |
|cheY3	|cheY3	|RM4018p_19780	|Chemotaxis protein CheY	|ABU_RS09820	|response regulator	|372 |
|ureD	|ureD	|RM4018p_08100	|Urease accessory protein UreD	|ABU_RS04030	|Urease accessory protein UreD	|753 |
|ureA	|ureAB	|RM4018p_08110	|Urease subunit alpha 	|ABU_RS04035	|Urease subunit gamma	|678 |
|ureC	|ureC	|RM4018p_08120	|Urease subunit alpha 	|ABU_RS04040	|Urease subunit alpha	|1,701 |
|ureE	|ureE	|RM4018p_08130	|Urease accessory protein UreE	|ABU_RS04045	|Urease accessory protein UreE	|447 |
|ureF	|ureF	|RM4018p_08140	|Urease accessory protein UreF	|ABU_RS04050	|Urease 	|687 |
|ureG	|ureG	|RM4018p_08150	|Urease accessory protein UreG	|ABU_RS04055	|Urease accessory protein UreG	|588 |
|besA	|iroE	|RM4018p_07320	|Ferri-bacillibactin esterase BesA	|ABU_RS03640	|alpha/beta hydrolase	|870 |
|bvgS	|-	|RM4018p_10320	|Virulence sensor protein BvgS	|ABU_RS05140	|transporter substrate-binding domain-containing protein	|1,575 |
|ccp	|docA	|RM4018p_08900	|Cytochrome c551 peroxidase	|ABU_RS04435	|c-type cytochrome	|918 |
|cdiA1	|hecA	|RM4018p_09450	|16S rRNA endonuclease CdiA	|ABU_RS04710	|filamentous hemagglutinin N-terminal domain-containing protein	|9,102 |
|ciaB	|ciaB	|RM4018p_15560	|Hypothetical protein	|ABU_RS07735	|invasion protein CiaB	|1,884 |
|cirA1	|irgA	|RM4018p_07310	|Colicin I receptor	|ABU_RS03635	|TonB-dependent receptor	|2,091 |
|cirA2	|-	|RM4018p_20030	|Colicin I receptor	|ABU_RS09945	|TonB-dependent receptor	|2,064 |
|cirA3	|cfrB	|L353p_13840	|Colicin I receptor	|-	|-	|2,289 |
|cirA4	|-	|L353p_19620	|Colicin I receptor	|-	|-	|2,034 |
|cj1349	|cj1349	|RM4018p_00690	|Hypothetical protein	|ABU_RS00335	|DUF814 domain-containing protein	|1,329 |
|cvfB	|cvfB	|RM4018p_05590	|conserved virulence factor B	|ABU_RS02785	|DNA-binding protein	|825 |
|degP	|htrA	|RM4018p_21080	|Periplasmic serine endoprotease DegP	|ABU_RS10475	|Do family serine endopeptidase	|1,428 |
|fur	|fur	|RM4018p_17830	|Ferric uptake regulation protein	|ABU_RS08850	|transcriptional repressor	|450 |
|iamA	|iamA	|RM4018p_01090	|putative ribonucleotide transport ATP-binding protein (mkl)	|ABU_RS00540	|ATP-binding cassette domain-containing protein	|738 |
|luxS	|luxS	|RM4018p_01130	|S-ribosylhomocysteine lyase	|ABU_RS00560	|S-ribosylhomocysteine lyase	|516 |
|murJ	|mviN	|RM4018p_08820	|Lipid II flippase MurJ	|ABU_RS04395	|murein biosynthesis integral membrane protein MurJ	|1,302 |
|oprF2	|cadF	|RM4018p_04820	|Outer membrane porin F	|ABU_RS02405	|OmpA family protein	|1,017 |
|phoP1	|-	|RM4018p_06330	|Transcriptional regulatory protein PhoP	|ABU_RS03155	|response regulator transcription factor	|693 |
|phoP2	|-	|RM4018p_15910	|Transcriptional regulatory protein PhoP	|ABU_RS07910	|response regulator transcription factor	|681 |
|phoP3	|-	|RM4018p_18810	|Virulence transcriptional regulatory protein PhoP	|ABU_RS09330	|response regulator transcription factor	|681 |
|phoQ	|phoQ	|RM4018p_10420	|sensor protein	|ABU_RS05190	|sensor histidine kinase	|1,908 |
|pldA	|pldA	|RM4018p_08650	|Putative phospholipase A1	|ABU_RS04310	|phospholipase A	|1,011 |
|rfaC	|waaC	|RM4018p_18340	|Lipopolysaccharide heptosyltransferase 1 (rfaC)	|ABU_RS09110	|lipopolysaccharide heptosyltransferase I (waaC)	|1,005 |
|rfaF	|waaF	|RM4018p_18120	|ADP-heptose--LPS heptosyltransferase 2 (rfaF)	|ABU_RS09000	|glycosyltransferase family 9 protein (waaF)	|939 |
|shlB	|hecB	|RM4018p_09440	|Hemolysin transporter protein ShlB	|ABU_RS04705	|ShlB/FhaC/HecB family hemolysin secretion/activation protein	|1,629 |
|tlyA	|tlyA	|RM4018p_18480	|16S/23S rRNA (cytidine-2'-O)-methyltransferase TlyA	|ABU_RS09170	|TlyA family rRNA methyltransferase	|699 |
|virF	|virF	|BMH_AB_233Bp_11740	|Virulence regulon transcriptional activator VirF	|ABU_RS07855	|AraC family transcriptional regulator	|786 |
|voc	|VOC	|RM4018p_20810	|Virulence protein	|ABU_RS10345	|VOC family protein	|381 |
|fcl	|-	|Ab_4511p_20030	|GDP-L-fucose synthase	|-	|-	|936 |
|glmM2	|-	|Ab_4511p_19970	|Phosphoglucosamine mutase	|-	|-	|1,434 |
|gmhA	|gmhA2	|Ab_4511p_20000	|Phosphoheptose_isomerase	|-	|-	|585 |
|gmhB2	|-	|Ab_4511p_20020	|D-glycero-alpha-D-manno-heptose-1,7-bisphosphate 7-phosphatase	|-	|-	|516 |
|hddA	|-	|Ab_4511p_19990	|D-glycero-alpha-D-manno-heptose 7-phosphate kinase	|-	|-	|1,023 |
|hddC	|-	|Ab_4511p_20010	|D-glycero-alpha-D-manno-heptose 1-phosphate guanylyltransferase	|-	|-	|702 |
|rmd	|-	|Ab_4511p_19980	|GDP-6-deoxy-D-mannose reductase	|-	|-	|1,041 |
|waaA	|-	|RM4018p_19870	|3-deoxy-D-manno-octulosonic acid transferase	|ABU_RS09865	|3-deoxy-D-manno-octulosonic acid transferase	|1,152 |
|lpxA	|-	|RM4018p_22260	|Acyl-(acyl-carrier-protein)--UDP-N-acetylglucosamine O-acetyltransferase	|ABU_RS11050	|acyl-ACP--UDP-N-acetylglucosamine O-acyltransferase	|783 |
|lpxB	|-	|RM4018p_22280	|Lipid-A-disaccharide synthase	|ABU_RS11060	|Lipid-A-disaccharide synthase	|1,044 |
|lpxC	|-	|RM4018p_20490	|UDP-3-O-acyl-N-acetylglucosamine deacetylase	|ABU_RS10180	|UDP-3-O-acyl-N-acetylglucosamine deacetylase	|906 |
|lpxD	|-	|RM4018p_11650	|UDP-3-O-acylglucosamine-N-acyltransferase	|ABU_RS05800	|UDP-3-O-(3-hydroxymyristoyl) glucosamine N-acyltransferase	|948 |
|lpxH	|-	|RM4018p_06230	|UDP-2,3-diacylglucosamine hydrolase	|ABU_RS03105	|Hypothetical protein	|699 |
|lpxK	|-	|RM4018p_12540	|Tetraacyldisaccharide 4'-kinase	|ABU_RS06225	|tetraacyldisaccharide 4'-kinase	|945 |
|lpxP	|-	|RM4018p_18330	|Lipid A biosynthesis palmitoleoyltransferase	|ABU_RS09105	|lipid A biosynthesis acyltransferase	|915 |
|cssS	|-	|D4963p_10540	|Sensor histidine kinase CssS	|-	|-	|1,737 |
|cusR	|-	|D4963p_10550	|Transcriptional regulatory protein CusR	|-	|-	|657 |
|-	|-	|D4963p_10560	|3'3'-cGAMP-specific phosphodiesterase 2	|-	|-	|2,196 |
|tap4	|-	|D4963p_10570	|Methyl-accepting chemotaxis protein IV	|-	|-	|2,232 |
|-	|-	|D4963p_10580	|Hypothetical protein	|-	|-	|243 |
|hipA2	|hlpA	|D4963p_10590	|Serine/threonine-protein kinase HipA	|-	|-	|1,278 |
|-	|-	|D4963p_10600	|Hypothetical protein	|-	|-	213
|hipA3	|hlpA	|D4963p_10610	|Serine/threonine-protein kinase HipA	|-	|-	|327 |
|hipA4	|hlpA	|D4963p_10620	|Serine/threonine-protein kinase HipA	|-	|-	|1,014 |
|-	|-	|D4963p_10630	|Hypothetical protein	|-	|-	|315 |
|-	|-	|D4963p_10640	|Hypothetical protein	|-	|-	|411 |
|-	|-	|D4963p_10650	|Hypothetical protein	|-	|-	|279 |
|-	|-	|D4963p_10660	|Hypothetical protein	|-	|-	|558 |
|-	|-	|D4963p_10670	|Hypothetical protein	|-	|-	|1,227 |
|-	|-	|D4963p_10680	|Hypothetical protein	|-	|-	|600 |
|-	|-	|D4963p_10690	|Hypothetical protein	|-	|-	|258 |
|-	|-	|D4963p_10700	|Hypothetical protein	|-	|-	|147 |
|-	|-	|D4963p_10710	|Hypothetical protein	|-	|-	|195 |
|-	|-	|D4963p_10720	|Hypothetical protein	|-	|-	|654 |
|-	|-	|D4963p_10730	|Hypothetical protein	|-	|-	|198 |
|-	|-	|D4963p_10740	|Hypothetical protein	|-	|-	|5,706 |
|topB	|-	|D4963p_10750	|DNA topoisomerase 3	|-	|-	|2,211 |
|-	|-	|D4963p_10760	|Hypothetical protein	|-	|-	|351 |
|virB1	|virB1	|D4963p_10770	|Type IV secretion system protein virB1	|-	|-	|492 |
|-	|-	|D4963p_10780	|Hypothetical protein	|-	|-	|432 |
|-	|-	|D4963p_10790	|Hypothetical protein	|-	|-	|531 |
|-	|-	|D4963p_10800	|Hypothetical protein	|-	|-	|510 |
|-	|-	|D4963p_10810	|Hypothetical protein	|-	|-	|5,526 |
|dnaG1	|-	|D4963p_10820	|DNA primase	|-	|-	|1,092 |
|-	|-	|D4963p_10830	|Hypothetical protein	|-	|-	|183 |
|-	|-	|D4963p_10840	|Hypothetical protein	|-	|-	|708 |
|-	|-	|D4963p_10850	|Hypothetical protein	|-	|-	|663 |
|traG	|virD4	|D4963p_10860	|Conjugal transfer protein TraG	|-	|-	|1,842 |
|virB11	|virB11	|D4963p_10870	|Type IV secretion system protein VirB11	|-	|-	|1,047 |
|virB10	|virB10	|D4963p_10880	|Type IV secretion system protein virB10	|-	|-	|1,164 |
|virB9	|virB9	|D4963p_10890	|Type IV secretion system protein virB9	|-	|-	|888 |
|virB8	|virB8	|D4963p_10900	|Type IV secretion system protein virB8	|-	|-	|693 |
|-	|-	|D4963p_10910	|Hypothetical protein	|-	|-	|186 |
|-	|-	|D4963p_10920	|Hypothetical protein	|-	|-	|405 |
|-	|virB6	|D4963p_10930	|Hypothetical protein	|-	|-	|915 |
|-	|-	|D4963p_10940	|Hypothetical protein	|-	|-	|282 |
|-	|virB5	|D4963p_10950	|Hypothetical protein	|-	|-	|717 |
|-	|virB7	|D4963p_10960	|Hypothetical protein	|-	|-	|438 |
|-	|-	|D4963p_10970	|Hypothetical protein	|-	|-	|633 |
|virB4	|virB4	|D4963p_10980	|Type IV secretion system protein virB4	|-	|-	|2,760 |
|-	|virB2	|D4963p_10990	|Hypothetical protein	|-	|-	|294 |
|-	|-	|D4963p_11000	|Hypothetical protein	|-	|-	|192 |
|-	|-	|D4963p_11010	|Hypothetical protein	|-	|-	|1,206 |
|-	|-	|D4963p_11020	|Hypothetical protein	|-	|-	|291 |
|-	|-	|D4963p_11030	|Hypothetical protein	|-	|-	|399 |
|-	|-	|D4963p_11040	|Hypothetical protein	|-	|-	|186 |
|-	|-	|D4963p_11050	|Hypothetical protein	|-	|-	|414 |
|-	|-	|D4963p_11060	|Hypothetical protein	|-	|-	|315 |
|-	|-	|D4963p_11070	|Hypothetical protein	|-	|-	|624 |
|intA1	|-	|D4963p_11080	|Prophage CP4-57 integrase	|-	|-	|1,254 |


# References 
* Müller Eva, Abdel-Glil Mostafa, Hotzel Helmut, Hänel Ingrid, Tomaso Herbert. 2020. **_Aliarcobacter butzleri_ from Water Poultry: Insights into Antimicrobial Resistance, Virulence and Heavy Metal Resistance**. Genes | [doi: 10.3390/genes11091104](https://doi.org/10.3390/genes11091104)

* Müller Eva, Hotzel Helmut, Linde Jörg, Hänel Ingrid, Tomaso Herbert. 2020. **Antimicrobial Resistance and in silico Virulence Profiling of _Aliarcobacter butzleri_ Strains From German Water Poultry**. Frontiers in Microbiology | [doi: 10.3389/fmicb.2020.617685](https://www.frontiersin.org/articles/10.3389/fmicb.2020.617685/full)


* Müller Eva, Hotzel Helmut, Ahlers Christine, Hänel Ingrid, Tomaso Herbert, Abdel-Glil Mostafa Y. 2020. **Genomic Analysis and Antimicrobial Resistance of _Aliarcobacter cryaerophilus_ Strains From German Water Poultry**. Frontiers in Microbiology | [doi: 10.3389/fmicb.2020.01549](https://www.frontiersin.org/articles/10.3389/fmicb.2020.01549/full)
